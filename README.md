# A book with some lines

Hey, change the `nbrlines` param in `js/main.js`!

## Context

Made in 2018 at Ensad for paged.js × PrePostPrint event.

## License

[GNU general public license V3](https://tldrlegal.com/license/gnu-general-public-license-v3-(gpl-3))
